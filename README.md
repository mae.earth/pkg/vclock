# vclock
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/mae.earth/pkg/vclock)](https://goreportcard.com/report/gitlab.com/mae.earth/pkg/vclock)

Vector Clock implementation in golang. 

```go

clock := New().Update(NewTag("Alice",1)).Update(NewTag("Fred",5))
fmt.Printf("%s\n",clock.Format(Delimiter)
```

```bash
Alice=1;Fred=5
```

```go

c0 := New().Update(NewTag("Alice",1)).Update(NewTag("Fred",1))
c1,err := Parse("Alice=3;Fred=1;Samantha=4",Delimiter)
if err != nil {
	panic(err)
}

fmt.Printf("c0:Alice=%d, c1:Alice=%d (should be a difference of 2)\n", c0.Tag("Alice",-1),c1.Tag("Alice",-1))

if c0.Equal(c1) {
	panic("should not be the case")
}

switch c0.RelationshipTo(c1) {
	case Ancestor:
		fmt.Printf("%q is an ancestor to %q\n",c0,c1)
	break
	case Descendant:
		fmt.Printf("%q is a descendant of %q\n",c0,c1)
	break
	case Sibling:
		fmt.Printf("%q is a sibling with %q\n",c0,c1)
	break
	case Unknown:
		fmt.Printf("%q is unrelated to %q\n",c0,c1)
	break
} 

c2 := c0.Update(NewTag("Alice",2))

fmt.Printf("c0:Alice=%d, c2:Alice=%d\n",c0.Tag("Alice",-1),c2.Tag("Alice",-1))

if c2.RelationshipIs(c0,Descendant) {
	fmt.Printf("%q is a descendant of %q\n",c2,c0)
} 

if c0.RelationshipIs(c2,Ancestor) {
	fmt.Printf("%q is an ancestor to %q\n",c0,c2)
}

c2 = c2.Update(NewTag("Fred",2)).Update(NewTag("Daisy",1))

c3 := c1.Merge(c2)
fmt.Printf("%q\n",c3)

```

```bash
c0:Alice=1, c1:Alice=3 (should be a difference of 2)
"Alice=1;Fred=1" is an ancestor to "Alice=3;Fred=1;Samantha=4"
c0:Alice=1, c2:Alice=2
"Alice=2;Fred=1" is a descendant of "Alice=1;Fred=1"
"Alice=1;Fred=1" is an ancestor to "Alice=2;Fred=1"
"Alice=3;Fred=1;Samantha=4" + "Alice=2;Daisy=1;Fred=2" = "Alice=3;Daisy=1;Fred=2;Samantha=4"

```


