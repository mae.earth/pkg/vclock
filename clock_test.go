// mae.earth/pkg/vclock/clock_test.go
// mae 12016
package vclock

import (
	"crypto/rand"
	"fmt"
	. "github.com/smartystreets/goconvey/convey"
	"io/ioutil"
	"sort"
	"strings"
	"sync"
	"testing"
)

const genFuzzExport = false /* set to true to export initial corpus for fuzzing */

const testwords string = `
tanning;oink;jugular;drainer;zoologist;service;congrats;bloating
exalted;fraction;crewmate;straddle;verbally;jarring;neatly;superhero
mama;balmy;agreeably;glorious;unsecured;scoff;campus;repent
snitch;purplish;reawake;bullion;shrouded;compress;unneeded;flashbulb
bath;bouncy;disk;wipe;eating;shown;squealer;variety
retrial;jigsaw;judiciary;elongated;sitter;reheat;tree;outweigh
yearling;duller;deflected;spring;latticed;maritime;manor;slider
barbecue;ragweed;preflight;pummel;cauterize;disband;prevail;reattach
lazy;democrat;scam;kite;straw;swimming;trillion;donor
obsessed;reliably;wheat;student;almost;espionage;calamity;pancake
treble;pacify;channel;risk;exploit;glitzy;dial;stool
tidbit;twig;perplexed;goggles;single;entwine;deuce;overthrow
elusive;pretense;yarn;voyage;eradicate;oxidant;marigold;trivial
mutation;contend;fester;reviver;reproach;skintight;synapse;ditzy
overflow;aloof;consensus;untold;concur;dazzler;superjet;lullaby
oak;thermal;growing;subject;tightly;defiance;contour;skyrocket
relieve;transfer;overpower;football;campsite;cogwheel;overpower;daytime
shaking;drizzle;thumping;exclude;steadily;devouring;swifter;girdle
anyone;riches;praising;disarray;arguably;puritan;chloride;camera
busload;blooming;corset;feminine;embody;papaya;elated;nearly
cognition;sandlot;lucrative;heroics;groin;timothy;outcome;contour
threefold;pointed;gigolo;java;covenant;murkiness;clobber;heroism
untoasted;old;pancreas;arguably;uninsured;getting;illicitly;outback
splashed;feel;wisplike;tiling;uprising;facebook;autopilot;lasso
hastiness;unwoven;hardcover;fretted;coerce;retrieval;rumor;silliness
brush;saved;lividly;antihero;varying;squealer;awaken;plating
barstool;enjoying;brussels;viewer;wolverine;silo;escapable;another
lizard;monetize;clay;legibly;cauterize;gallon;animation;occupier
giddily;thirstily;why;amusing;blatantly;mashed;wildly;reveal
mascot;monogamy;captive;palm;refried;trapeze;handshake;unmatched
clavicle;viewless;entertain;plant;luckless;flock;headboard;jailbird
subdued;such;roping;rumor;unearth;replay;catchable;massager
`

var _internal struct {
	sync.RWMutex
	words [256]string
}

func genwords(count int, delimiter rune) string {
	_internal.RLock()
	defer _internal.RUnlock()

	if count <= 0 {
		return ""
	}

	r := make([]byte, count*4)
	rand.Read(r)

	w := make(map[string]int, 0)

	i := 0
	for {
		if i >= count*4 || len(w) >= count {
			break
		}
		an0 := int(r[i+0]) % len(_internal.words)
		an1 := int(r[i+1]) % len(_internal.words)
		an2 := int(r[i+2]) % len(_internal.words)
		word := _internal.words[an0][:2] + "-" + _internal.words[an1][:2] + "-" + _internal.words[an2][:2]
		w[word] = 1

		i += 3
	}

	out := make([]string, 0)
	for word := range w {
		out = append(out, word)
	}

	sort.Sort(ByAlpha(out))
	form := ""
	tagged := make(map[string]int, 0)
	for _, word := range out {
		if _, exists := tagged[word]; !exists {
			tagged[word] = 0
		}
		tagged[word]++
	}

	out = make([]string, 0)
	for word := range tagged {
		out = append(out, word)
	}

	sort.Sort(ByAlpha(out))

	for i, word := range out {
		form += fmt.Sprintf("%s=%d", word, i)
		if i < len(w)-1 {
			form += string(delimiter)
		}
	}
	return form
}

func init() {

	lines := strings.Split(testwords, "\n")
	if len(lines) != 34 {
		panic(fmt.Errorf("invalid test words, was only %d lines (expecting 34)", len(lines)))
	}

	count := 0
	for _, line := range lines[1:33] {
		w := strings.Split(line, ";")
		if len(w) != 8 {
			panic(fmt.Errorf("invalid test words, line had %d words (expecting 8)", len(w)))
		}

		for _, word := range w {
			_internal.words[count] = word
			count++
		}
	}
}

func Test_Genwords(t *testing.T) {

	Convey("Genwords", t, func() {

		w := genwords(5, ';')
		So(len(w), ShouldEqual, 54)

		fmt.Printf("genword = %q\n", w)
	})
}

func Test_Tag(t *testing.T) {

	Convey("Tag", t, func() {
		tag := NewTag("hello", 1)
		So(tag.User, ShouldEqual, "hello")
		So(tag.Version, ShouldEqual, 1)

		Convey("Next", func() {
			next := tag.Next()
			So(next.User, ShouldEqual, "hello")
			So(next.Version, ShouldEqual, 2)
		})

		Convey("Previous", func() {
			prev := tag.Previous()
			So(prev.User, ShouldEqual, "hello")
			So(prev.Version, ShouldEqual, 0)
		})
	})
}

func Test_Clock(t *testing.T) {

	Convey("Clock Parsing", t, func() {
		Convey("dups", func() {
			form := "alice=1;alice=2;"
			clock, err := Parse(form, Delimiter)
			So(err, ShouldBeNil)
			So(len(clock), ShouldEqual, 1)
			So(clock.Format(Delimiter), ShouldEqual, "alice=2")
		})

		Convey("null", func() {
			form := "=2;"
			clock, err := Parse(form, Delimiter)
			So(err, ShouldBeNil)
			So(len(clock), ShouldEqual, 0)
		})

	})

	Convey("Clock", t, func() {

		tag := NewTag("hello", 1)
		So(tag, ShouldNotBeNil)
		So(string(tag.User), ShouldEqual, "hello")
		So(tag.Version, ShouldEqual, 1)

		clock := New()
		So(clock, ShouldNotBeNil)
		So(len(clock), ShouldEqual, 0)

		clock = clock.Update(tag)
		So(clock, ShouldNotBeNil)
		So(len(clock), ShouldEqual, 1)
		So(clock.Format(Delimiter), ShouldEqual, "hello=1")

		clock = clock.Update(tag)
		So(len(clock), ShouldEqual, 1)
		So(clock.Format(Delimiter), ShouldEqual, "hello=1")

		tag = NewTag("hello", 2)
		clock = clock.Update(tag)

		tag = NewTag("bob", 1)
		So(tag, ShouldNotBeNil)
		So(string(tag.User), ShouldEqual, "bob")
		So(tag.Version, ShouldEqual, 1)

		clock1 := clock.Update(tag)
		So(clock1, ShouldNotBeNil)
		So(len(clock1), ShouldEqual, 2)

		So(clock1.Equal(clock), ShouldBeFalse)
		So(clock.Equal(clock), ShouldBeTrue)

		formated := clock1.Format(Delimiter)
		So(formated, ShouldEqual, "bob=1;hello=2")

		clock1c, err := Parse(formated, Delimiter)
		So(err, ShouldBeNil)
		So(clock1c, ShouldNotBeNil)
		So(len(clock1c), ShouldEqual, len(clock1))
		So(clock1c.Equal(clock1), ShouldBeTrue)

		So(clock.RelationshipTo(clock1), ShouldEqual, Ancestor)
		So(clock1.RelationshipTo(clock), ShouldEqual, Descendant)
		So(clock.RelationshipTo(clock), ShouldEqual, Sibling)

		So(clock1.RelationshipIs(clock, Ancestor), ShouldBeFalse)
		So(clock1.RelationshipIs(clock, Sibling), ShouldBeFalse)
		So(clock1.RelationshipIs(clock, Descendant), ShouldBeTrue)

	})

	Convey("Clock Tags", t, func() {
		a, _ := Parse("alice=1;in=2;wonderland=3", Delimiter)
		Convey("HasTag", func() {
			So(a.HasTag("alice"), ShouldBeTrue)
			So(a.HasTag("Alice"), ShouldBeFalse)
		})
		Convey("HasTags", func() {
			So(a.HasTags("Alice", "IN", "wonderland"), ShouldBeFalse)
			So(a.HasTags("alice", "in", "wonderland", "again"), ShouldBeFalse)
			So(a.HasTags("wonderland", "in", "alice"), ShouldBeTrue)
		})
		Convey("Tag", func() {
			So(a.Tag("alice", -1), ShouldEqual, 1)
			So(a.Tag("Alice", -1), ShouldEqual, -1)
			So(a.Tag("wonderland", -1), ShouldEqual, 3)
		})

	})

	Convey("Clock Relationship", t, func() {

		/* construct two unrelated clocks */
		a, _ := Parse("alice=1;in=2;wonderland=3", Delimiter)
		b, _ := Parse("red=1;blue=3", Delimiter)

		So(a.Equal(b), ShouldBeFalse)

		So(a.RelationshipTo(b), ShouldEqual, Unrelated)

		/* construct two clones */
		b, _ = Parse("alice=1;in=2;wonderland=3", Delimiter)
		So(a.RelationshipTo(b), ShouldEqual, Sibling)
		So(b.RelationshipTo(a), ShouldEqual, Sibling)

		/* descendant */
		b, _ = Parse("alice=1;in=2;wonderland=4", Delimiter)
		So(a.RelationshipTo(b), ShouldEqual, Ancestor)
		So(b.RelationshipTo(a), ShouldEqual, Descendant)

		/* next generation */
		c := b.Update(NewTag("WhiteRabbit", 1))
		So(a.RelationshipTo(c), ShouldEqual, Ancestor)
		So(b.RelationshipTo(c), ShouldEqual, Ancestor)
		So(c.RelationshipTo(b), ShouldEqual, Descendant)

		d, _ := Parse("alice=2;in=1;wonderland=5", Delimiter)
		So(d.RelationshipTo(c), ShouldEqual, Ancestor)
		So(d.RelationshipTo(a), ShouldEqual, Unknown)

		Convey("Merge", func() {
			e := c.Merge(d)
			So(len(e), ShouldEqual, 4)
			So(e.Format(Delimiter), ShouldEqual, "WhiteRabbit=1;alice=2;in=2;wonderland=5")
		})

	})

	Convey("Clock sizes", t, func() {

		sizes := []int{3, 10, 50, 100, 500, 1000, 5000, 10000}

		for _, size := range sizes {
			form := genwords(size, Delimiter)
			c, _ := Parse(form, Delimiter)
			out := c.Format(Delimiter)

			fmt.Printf("clock len=%d\t%dbytes\n", len(c), len(out))
		}
	})

	Convey("Fuzz export", t, func() {

		if !genFuzzExport {
			t.Skip()
		}

		form := genwords(10000, Delimiter)
		c, err := Parse(form, Delimiter)
		So(err, ShouldBeNil)
		out := c.Format(Delimiter)

		So(ioutil.WriteFile("exported", []byte(out), 0664), ShouldBeNil)

	})

	Convey("Example", t, func() {

		c0 := New().Update(NewTag("Alice", 1)).Update(NewTag("Fred", 1))
		c1, err := Parse("Alice=3;Fred=1;Samantha=4", Delimiter)
		So(err, ShouldBeNil)

		fmt.Printf("c0:Alice=%d, c1:Alice=%d (should be a difference of 2)\n", c0.Tag("Alice", -1), c1.Tag("Alice", -1))

		So(c0.Equal(c1), ShouldBeFalse)

		switch c0.RelationshipTo(c1) {
		case Ancestor:
			fmt.Printf("%q is an ancestor to %q\n", c0, c1)
			break
		case Descendant:
			fmt.Printf("%q is a descendant of %q\n", c0, c1)
			break
		case Sibling:
			fmt.Printf("%q is a sibling with %q\n", c0, c1)
			break
		case Unknown:
			fmt.Printf("%q is unrelated to %q\n", c0, c1)
			break
		}

		c2 := c0.Update(NewTag("Alice", 2))

		fmt.Printf("c0:Alice=%d, c2:Alice=%d\n", c0.Tag("Alice", -1), c2.Tag("Alice", -1))

		if c2.RelationshipIs(c0, Descendant) {
			fmt.Printf("%q is a descendant of %q\n", c2, c0)
		}

		if c0.RelationshipIs(c2, Ancestor) {
			fmt.Printf("%q is an ancestor to %q\n", c0, c2)
		}

		c2 = c2.Update(NewTag("Fred", 2)).Update(NewTag("Daisy", 1))

		c3 := c1.Merge(c2)
		fmt.Printf("%q + %q = %q\n", c1, c2, c3)
	})
}

type ByAlpha []string

func (ba ByAlpha) Len() int           { return len(ba) }
func (ba ByAlpha) Less(i, j int) bool { return (ba[i] < ba[j]) }
func (ba ByAlpha) Swap(i, j int)      { ba[i], ba[j] = ba[j], ba[i] }

/* go test -bench=. */
func Benchmark_Parse(ben *testing.B) {
	for i := 0; i < ben.N; i++ {
		a, err := Parse("alice=1;in=2;wonderland=3", Delimiter)
		if len(a) != 3 {
			ben.Errorf("invalid parse")
		}
		if err != nil {
			ben.Errorf("invalid parse -- %v", err)
		}
	}
}

/* 10 is probably going to be the sweat spot for number of id's in use */
func Benchmark_Parse10(ben *testing.B) {
	ben.StopTimer()
	form := genwords(10, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		_, err := Parse(form, Delimiter)
		if err != nil {
			ben.Errorf("invalid parse -- %v", err)
		}
	}
}

func Benchmark_ParseLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(100, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		_, err := Parse(form, Delimiter)
		if err != nil {
			ben.Errorf("invalid parse -- %v", err)
		}
	}
}

func Benchmark_ParseVeryLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(1000, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		_, err := Parse(form, Delimiter)
		if err != nil {
			ben.Errorf("invalid parse -- %v", err)
		}
	}
}

func Benchmark_Format(ben *testing.B) {
	form := "alice=1;in=2;wonderland=3"
	c, _ := Parse(form, Delimiter)
	for i := 0; i < ben.N; i++ {
		str := c.Format(Delimiter)
		if str != form {
			ben.Errorf("invalid format")
		}
	}
}

func Benchmark_Format10(ben *testing.B) {
	ben.StopTimer()
	form := genwords(10, Delimiter)
	c, _ := Parse(form, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		str := c.Format(Delimiter)
		if str != form {
			ben.Errorf("invalid format\n[%s]\n[%s]", str, form)
		}
	}
}

func Benchmark_FormatLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(100, Delimiter)
	c, _ := Parse(form, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		str := c.Format(Delimiter)
		if str != form {
			ben.Errorf("invalid format\n[%s]\n[%s]", str, form)
		}
	}
}

func Benchmark_FormatVeryLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(1000, Delimiter)
	c, _ := Parse(form, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		str := c.Format(Delimiter)
		if str != form {
			ben.Errorf("invalid format\n[%s]\n[%s]", str, form)
		}
	}
}

func Benchmark_Merge(ben *testing.B) {
	a, _ := Parse("alice=1;in=2;wonderland=3", Delimiter)
	b, _ := Parse("red=1;blue=2;yellow=3;brown=4;", Delimiter)

	for i := 0; i < ben.N; i++ {
		c := a.Merge(b)
		if len(c) != 7 {
			ben.Errorf("invalid merge")
		}
	}
}

func Benchmark_Merge10(ben *testing.B) {
	ben.StopTimer()
	form0 := genwords(5, Delimiter)
	form1 := genwords(5, Delimiter)
	a, _ := Parse(form0, Delimiter)
	b, _ := Parse(form1, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		c := a.Merge(b)
		if len(c) < 10 {
			ben.Errorf("invalid merge\n[%s]\n[%s]\n", form0, form1)
		}
	}
}

func Benchmark_MergeLarge(ben *testing.B) {
	ben.StopTimer()
	form0 := genwords(100, Delimiter)
	form1 := genwords(100, Delimiter)
	a, _ := Parse(form0, Delimiter)
	b, _ := Parse(form1, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		c := a.Merge(b)
		if len(c) < 10 {
			ben.Errorf("invalid merge")
		}
	}
}

func Benchmark_MergeVeryLarge(ben *testing.B) {
	ben.StopTimer()
	form0 := genwords(1000, Delimiter)
	form1 := genwords(1000, Delimiter)
	a, _ := Parse(form0, Delimiter)
	b, _ := Parse(form1, Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		c := a.Merge(b)
		if len(c) < 100 {
			ben.Errorf("invalid merge")
		}
	}
}

func Benchmark_Update(ben *testing.B) {
	ben.StopTimer()
	c, _ := Parse("alice=1;in=2;wonderland=3", Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag("Madhatter", 1)
		c = c.Update(tag)
		if len(c) != count+1 {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_Update10(ben *testing.B) {
	ben.StopTimer()
	form := genwords(10, Delimiter)
	c, _ := Parse(form, Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag("Madhatter", 1)
		c = c.Update(tag)
		if len(c) != count+1 {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_UpdateExisting(ben *testing.B) {
	ben.StopTimer()
	c, _ := Parse("alice=1;in=2;wonderland=3", Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag("alice", 2)
		c = c.Update(tag)
		if len(c) != count {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_UpdateExisting10(ben *testing.B) {
	ben.StopTimer()
	form := genwords(10, Delimiter)
	c, _ := Parse(form, Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag(c[0].User, c[0].Version+1)
		c = c.Update(tag)
		if len(c) != count {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_UpdateLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(100, Delimiter)
	c, _ := Parse(form, Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag("Madhatter", 1)
		c = c.Update(tag)
		if len(c) != count+1 {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_UpdateExistingLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(100, Delimiter)
	c, _ := Parse(form, Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag(c[0].User, c[0].Version+1)
		c = c.Update(tag)
		if len(c) != count {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_UpdateVeryLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(1000, Delimiter)
	c, _ := Parse(form, Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag("Madhatter", 1)
		c = c.Update(tag)
		if len(c) != count+1 {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_UpdateExistingVeryLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(1000, Delimiter)
	c, _ := Parse(form, Delimiter)
	count := len(c)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		tag := NewTag(c[0].User, c[0].Version+1)
		c = c.Update(tag)
		if len(c) != count {
			ben.Errorf("invalid update")
		}
	}
}

func Benchmark_Relationship(ben *testing.B) {
	ben.StopTimer()
	a, _ := Parse("alice=1;in=2;wonderland=3", Delimiter)
	b, _ := Parse("alice=2;in=2;wonderland=3", Delimiter)
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		if a.RelationshipTo(b) != Ancestor {
			ben.Errorf("invalid relationship")
		}
	}
}

func Benchmark_Relationship10(ben *testing.B) {
	ben.StopTimer()
	form := genwords(10, Delimiter)
	a, _ := Parse(form, Delimiter)
	b := a.Update(NewTag(a[0].User, a[0].Version+1))
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		if a.RelationshipTo(b) != Ancestor {
			ben.Errorf("invalid relationship")
		}
	}
}

func Benchmark_RelationshipLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(100, Delimiter)
	a, _ := Parse(form, Delimiter)
	b := a.Update(NewTag(a[0].User, a[0].Version+1))
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		if a.RelationshipTo(b) != Ancestor {
			ben.Errorf("invalid relationship")
		}
	}
}

func Benchmark_RelationshipVeryLarge(ben *testing.B) {
	ben.StopTimer()
	form := genwords(1000, Delimiter)
	a, _ := Parse(form, Delimiter)
	b := a.Update(NewTag(a[0].User, a[0].Version+1))
	ben.StartTimer()
	for i := 0; i < ben.N; i++ {
		if a.RelationshipTo(b) != Ancestor {
			ben.Errorf("invalid relationship")
		}
	}
}
