// mae.earth/pkg/vclock/clock.go
// mae 12016
// Package vclock implements vector clocks
package vclock

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

/* Relationship */
type Relationship byte

const (
	Delimiter = rune(';')

	Unknown    = Relationship(0)
	Ancestor   = Relationship(1)
	Descendant = Relationship(2)
	Sibling    = Relationship(3)
	Unrelated  = Relationship(4)
)

/* Tag */
type Tag struct {
	User    string
	Version int
}

/* Next */
func (t Tag) Next() Tag {
	return Tag{User: t.User, Version: t.Version + 1}
}

/* Previous */
func (t Tag) Previous() Tag {
	return Tag{User: t.User, Version: t.Version - 1}
}

/* NewTag */
func NewTag(user string, ver int) Tag {
	return Tag{User: user, Version: ver}
}

/* ByUser */
type ByUser []Tag

func (bu ByUser) Len() int           { return len(bu) }
func (bu ByUser) Less(i, j int) bool { return (bu[i].User < bu[j].User) }
func (bu ByUser) Swap(i, j int)      { bu[i], bu[j] = bu[j], bu[i] }

/* Clock */
type Clock []Tag

/* EmptyClock */
var EmptyClock = []Tag{}

/* Parse */
func Parse(clock string, delimiter rune) (Clock, error) {

	parts := strings.Split(clock, string(delimiter))
	tags := make([]Tag, 0)
	tagged := make(map[string]int, 0)

	for _, part := range parts {
		pair := strings.Split(part, "=")
		if len(pair) != 2 {
			continue
		}

		if len(pair[0]) == 0 {
			continue
		}

		i, err := strconv.Atoi(pair[1])
		if err != nil {
			return EmptyClock, err
		}

		if ver, exists := tagged[pair[0]]; exists {
			if i <= ver {
				continue
			}
		}
		tagged[pair[0]] = i
	}

	for user, ver := range tagged {
		if len(user) == 0 {
			continue
		}
		tags = append(tags, Tag{User: user, Version: ver})
	}

	sort.Sort(ByUser(tags))

	return Clock(tags), nil
}

/* Merge */
func (c Clock) Merge(other Clock) Clock {
	if len(c) == 0 && len(other) == 0 {
		return EmptyClock
	}

	vers := make(map[string]int, 0)
	for _, t := range c {
		vers[t.User] = t.Version
	}

	for _, t := range other {
		if ver, exists := vers[t.User]; exists {
			if t.Version <= ver {
				continue
			}
		}
		vers[t.User] = t.Version
	}

	tags := make([]Tag, 0)
	for u, v := range vers {
		tags = append(tags, Tag{User: u, Version: v})
	}

	sort.Sort(ByUser(tags))

	return Clock(tags)
}

/* Update */
func (c Clock) Update(tag Tag) Clock {

	/* NOTE: this is singular for a reason. */

	found := false
	for _, t := range c {
		if t.User == tag.User {
			found = true
			if t.Version == tag.Version {
				return c
			}
			break
		}
	}
	var tags []Tag

	if found {
		for _, t := range c {
			if t.User == tag.User {
				tags = append(tags, tag)
				continue
			}
			tags = append(tags, t)
		}
	} else {

		tags = make([]Tag, len(c))
		copy(tags, []Tag(c))
		tags = append(tags, tag)
	}

	sort.Sort(ByUser(tags))

	return Clock(tags)
}

/* String */
func (c Clock) String() string {
	return c.Format(Delimiter)
}

/* Format */
func (c Clock) Format(delimiter rune) string {
	out := ""
	for i, t := range c {
		if i > 0 {
			out += string(delimiter)
		}
		out += fmt.Sprintf("%s=%d", t.User, t.Version)
	}
	return out
}

/* Equal */
func (c Clock) Equal(other Clock) bool {
	if len(c) != len(other) {
		return false
	}

	for i := 0; i < len(other); i++ {
		if other[i].Version != c[i].Version {
			return false
		}
		if other[i].User != c[i].User {
			return false
		}
	}
	return true
}

/* RelationshipTo */
func (c Clock) RelationshipTo(other Clock) Relationship {
	/* do the sibling first */
	if c.Equal(other) {
		return Sibling
	}

	/* go through and ensure that both clocks have the same tags,
	 * if a tag is missing then add it @ 0, then try and determine
	 * the relationship between the two clocks; ancestor and descendant
	 * remaining - with an edge case of sibling if tags are not related
	 */

	a := make([]Tag, 0)
	b := make([]Tag, 0)

	var diff int

	for _, t := range c {
		a = append(a, t)
		found := false
		for _, t0 := range other {
			if t0.User == t.User {
				found = true
				break
			}
		}
		if !found {
			diff++
			b = append(b, Tag{User: t.User, Version: 0})
		}
	}
	if diff >= len(c) {
		return Unrelated
	}

	for _, t := range other {
		b = append(b, t)
		found := false
		for _, t0 := range c {
			if t0.User == t.User {
				found = true
				break
			}
		}
		if !found {
			a = append(a, Tag{User: t.User, Version: 0})
		}
	}

	sort.Sort(ByUser(a))
	sort.Sort(ByUser(b))

	/* NOTE: this is first order, very crude anyalsis
	 * FIXME needs formal check */
	var adiff = 0
	var bdiff = 0

	for i := 0; i < len(a); i++ {

		if a[i].Version > b[i].Version {
			adiff++
			continue
		}

		if a[i].Version < b[i].Version {
			bdiff++
			continue
		}
	}

	/*
		fmt.Printf("a [%v]\n",a)
		fmt.Printf("b [%v]\n",b)
		fmt.Printf("\tadiff = %d, bdiff = %d\n",adiff,bdiff)
	*/
	if adiff > bdiff {
		/* go through and look for edge cases */
		for i := 0; i < len(a); i++ {
			if a[i].Version == 0 || b[i].Version == 0 {
				continue
			}

			if a[i].Version < b[i].Version {
				return Unknown
			}
		}

		return Descendant
	}

	return Ancestor
}

/* RelationshipIs */
func (c Clock) RelationshipIs(other Clock, r Relationship) bool {
	return (c.RelationshipTo(other) == r)
}

/* Tag */
func (c Clock) Tag(tag string, def int) int {

	for _, t := range c {
		if t.User == tag {
			return t.Version
		}
	}

	return def
}

/* HasTag */
func (c Clock) HasTag(tag string) bool {

	for _, t := range c {
		if t.User == tag {
			return true
		}
	}

	return false
}

/* HasTags */
func (c Clock) HasTags(tags ...string) bool {

	for _, t := range tags {
		found := false
		for _, u := range c {
			if u.User == t {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}

	return true
}

/* New */
func New() Clock {
	return Clock([]Tag{})
}

/* Fuzz see https://github.com/dvyukov/go-fuzz */
func Fuzz(data []byte) int {
	if _, err := Parse(string(data), Delimiter); err != nil {
		return 0
	}
	return 1
}
